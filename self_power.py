import time
start_time = time.time()

def self_power(max_range, last_int):
    number = sum(x**x for x in range(1,max_range+1))
    return str(number)[-last_int:]

print(self_power(1000,10))

#Or in one line of code as below :
#print(str(sum(x**x for x in range(1,1000+1)))[-10:])
print(f"--- {(time.time() - start_time):.10f} seconds ---" )